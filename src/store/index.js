import Vue from 'vue'
import Vuex from 'vuex'
import *as actions from './actions'
import *as getters from './getters'
import state from './state'
import mutations from './mutations'
// 打印日志插件
import createLogger from 'vuex/dist/logger'


Vue.use(Vuex)
// 开发环境上用 线上就会耗性能 严格模式
const debug = process.env.NOOE_ENV = 'production'

export default new Vuex.Store({
    actions,
    getters,
    mutations,
    state,
    strict:debug,
    plugins:debug ? [createLogger()]:[]
})
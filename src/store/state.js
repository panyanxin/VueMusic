import { playMode } from 'common/js/config'

const state = {
    // 歌手
    singer: {},
    // 播放状态
    playing: false,
    // 全屏
    fullScreen: false,
    // 播放列表
    playlist: [],
    // 顺序播放
    sequenceList: [],
    // 播放模式
    mode: playMode.sequence,
    currentIndex: -1,
    disc: {}
}

export default state
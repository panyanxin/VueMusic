import { getLyric } from 'api/song'
import { ERR_OK } from 'api/config'
import { Base64 } from 'js-base64'

export default class Song {
    constructor({ id, mid, singer, name, album, duration, image, url }) {
        this.id = id
        this.mid = mid
        this.singer = singer
        this.name = name
        this.album = album
        this.duration = duration
        this.image = image
        this.url = url
    }

    getLyric() {
        if (this.lyric) {
            return Promise.resolve(this.lyric)
        }

        return new Promise((resolve, reject) => {
            getLyric(this.mid).then((res) => {
                if (res.retcode === ERR_OK) {
                    this.lyric = Base64.decode(res.lyric)
                    resolve(this.lyric)
                } else {
                    reject('no lyric')
                }
            })
        })
    }

}
export function createSong(musicData) {
    return new Song({
        id: musicData.songid,
        mid: musicData.songmid,
        singer: filterSinger(musicData.singer),
        name: musicData.songname,
        album: musicData.albumname,
        duration: musicData.interval,
        image: `https://y.gtimg.cn/music/photo_new/T002R300x300M000${musicData.albummid}.jpg?max_age=2592000`,
        // url: `http://ws.stream.qqmusic.qq.com/${musicData.strMediaMid}.m4a?fromtag=38`
        // url: `http://dl.stream.qqmusic.qq.com/C400${musicData.strMediaMid}.m4a?guid=2763554805&vkey=372F4FF5ED6CA116FFD84A5E276002B8089B9DB8154642B9BC8B684DB498483E0AB8E4EE72F3E3BF2218E0286057C5A86C32C9E8BD760698&uin=0&fromtag=38`
        url: `http://dl.stream.qqmusic.qq.com/C400${musicData.strMediaMid}.m4a?guid=2108114790&vkey=EC1FAD559C2D719A101CC8B40B4ED44CEC49255D1023611153AB4A4700762E1A7E000D959B6BB018C7656DF933DB7B4D4E14FA0274E18E25&uin=0&fromtag=38`
    })
}

function filterSinger(singer) {
    let ret = []
    if (!singer) {
        return ''
    }
    singer.forEach((s) => {
        ret.push(s.name)
    })
    return ret.join('/')
}